import { expect as _expect } from "chai";
import MongoDbConnection from "../server/Database/MongoDbConnection.js";
import DataBaseoperations from "../server/Database/databaseOperations/operations.js";
const expect = _expect;
import { databaseName, messageCollection, refrenceTable, topicCollection, data, topic, client } from "./constants.js";

const connection = new MongoDbConnection();
const operation = new DataBaseoperations();
let connect;
let messageToDb;
let topicTodb;

(async function () {
  connect = await connection.connectDb(databaseName);
})();

describe("operations on topics", () => {
  after(async () => {
    await connect.collection(topicCollection).drop();
  });

  it("Should add topic to db", async () => {
    topicTodb = await operation.addToDb(topic, topicCollection, connect);
    expect(topicTodb.ops[0].topic_id).to.equal("1");
  });

  it("should have property topic_id", async () => {
    expect(topicTodb.ops[0]).to.have.property("topic_id");
  });

  it("should have property topicName", async () => {
    expect(topicTodb.ops[0]).to.have.property("topicName");
  });

  it("Should get topics from db", async () => {
    const findTopics = await operation.getTopicsFromDb(connect, topicCollection);
    expect(findTopics).to.have.lengthOf(1);
  });

  it("Should get only one topics from db", async () => {
    const findTopics = await operation.getTopicsFromDb(connect, topicCollection);
    expect(findTopics).to.not.have.lengthOf(2);
  });
});

describe("add messages to database", async () => {
  it("should add message to db", async () => {
    messageToDb = await operation.addToDb(data, messageCollection, connect);
    expect(messageToDb.ops[0].topicId).to.equal("1");
  });

  it("should added message to db and have proper message", async () => {
    expect(messageToDb.ops[0].message).to.not.equal("wrong testing");
  });

  it("should added message to db and have proper message", async () => {
    expect(messageToDb.ops[0].message).to.equal("testing");
  });

  it("should add message to db and have proper topicId", async () => {
    expect(messageToDb.ops[0].topicId).to.not.equal("2");
  });

  it("should add message to db and have proper topicId", async () => {
    expect(messageToDb.ops[0].topicId).to.equal("1");
  });
});

describe("find topic details in db", () => {
  after(async () => {
    await connect.collection(messageCollection).drop();
  });

  it("should have property topic id", async () => {
    const find = await operation.findTopicDetailsInDb(data.topicId, connect, messageCollection);
    expect(find[0]).to.have.property("topicId");
  });

  it("should have property timeStamp", async () => {
    const find = await operation.findTopicDetailsInDb(data.topicId, connect, messageCollection);
    expect(find[0]).to.have.property("timeStamp");
  });

  it("should have property message", async () => {
    const find = await operation.findTopicDetailsInDb(data.topicId, connect, messageCollection);
    expect(find[0]).to.have.property("message");
  });

  it("should have topic id", async () => {
    const find = await operation.findTopicDetailsInDb(data.topicId, connect, messageCollection);
    expect(find[0].topicId).to.equal("1");
  });

  it("should return topic with searched topic id", async () => {
    const find = await operation.findTopicDetailsInDb(data.topicId, connect, messageCollection);
    expect(find[0].topicId).to.not.equal("5");
  });

  it("should have message", async () => {
    const find = await operation.findTopicDetailsInDb(data.topicId, connect, messageCollection);
    expect(find[0].message).to.equal("testing");
  });

  it("should have proper message", async () => {
    const find = await operation.findTopicDetailsInDb(data.topicId, connect, messageCollection);
    expect(find[0].message).to.not.equal("wrong testing");
  });
});

describe("operations on client", () => {
  after(async () => {
    await connect.collection(refrenceTable).drop();
  });

  it("should add client to db", async () => {
    const addClient = await operation.addToDb(client, refrenceTable, connect);
    expect(addClient.ops[0].clientName).to.equal("127.0.0.1_60978");
  });

  it("should find client single in db", async () => {
    const findClient = await operation.checkForClientDetails(client, connect, refrenceTable);
    expect(findClient).to.not.have.lengthOf(2);
  });

  it("should find client in db", async () => {
    const findClient = await operation.checkForClientDetails(client, connect, refrenceTable);
    expect(findClient).to.have.lengthOf(1);
  });
});
