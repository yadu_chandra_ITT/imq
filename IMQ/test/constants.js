export const databaseName = "IMQ_Testing";

export const messageCollection = "IMQmessageCollection";

export const refrenceTable = "IMQreference";

export const topicCollection = "IMQtopics";

export const data = {
  topicId: "1",
  message: "testing",
  timeStamp: "2/27/2021, 5:14:02 PM",
};

export const topic = {
  topic_id: "1",
  topicName: "demo",
};

export const client = {
  clientName: "127.0.0.1_60978",
};
