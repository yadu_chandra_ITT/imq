import * as net from "net";
import * as readline from "readline";
import PublisherController from "./controller/publisherController.js";
import { publisherPORT, allTopics } from "./Constants.js";

const publisherHandler = new PublisherController();

const publisherConsoleInteractions = readline.createInterface({
  input: process.stdin,
});

const publisherServerInteraction = new net.Socket();

publisherServerInteraction.connect(publisherPORT, process.argv[2], () => {
  console.log(`\nWelcome to IMQ Publisher port\n\nAll the avilable topics\n`);
});

publisherServerInteraction.on("data", (res) => {
  const converter = JSON.parse(res.toString("utf-8"));
  publisherHandler.responseFromServer(converter, publisherConsoleInteractions);
});

publisherServerInteraction.on("error", (error) => {
  console.log("You are disconnected from the Server:" + error.message);
});

publisherServerInteraction.write(JSON.stringify(allTopics));

publisherConsoleInteractions.on("line", (message) => {
  publisherHandler.handlePublisherEntries(publisherServerInteraction, publisherConsoleInteractions, message);
});

publisherConsoleInteractions.on("close", () => {
  publisherServerInteraction.end();
});
