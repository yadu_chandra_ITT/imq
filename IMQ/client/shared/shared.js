import SharedError from "../errorHandler/sharedErrorHandler.js";

export default class Shared {
  printTopics(topics) {
    try {
      console.log(`Topic ID   Topic Name`);
      topics.map((item) => {
        if (item.topicName) {
          console.log(` ${item.topic_id}         ${item.topicName}`);
        }
      });
    } catch (error) {
      throw new SharedError(`failed to printTopics because ${error.message}`);
    }
    console.log("");
  }

  printMessages(messages) {
    try {
      messages.data.map((item) => {
        console.log(`${item.message}`);
      });
    } catch (error) {
      throw new SharedError(`failed to printMessages because ${error.message}`);
    }
  }

  printServerFullMessage(message, clientRequest) {
    console.log(message);
    clientRequest.close();
  }

  getRequiredString(message) {
    try {
      let data = message.match(/"([^"]+)"/)[1].trim();
      return data;
    } catch (error) {
      throw new SharedError(`failed to get the string message because ${error.message}`);
    }
  }

  getTopicId(message) {
    try {
      const data = message.match(/\d+/g).toString();
      return data;
    } catch (error) {
      throw new SharedError(`failed to get topic ID because ${error.message}`);
    }
  }
}
