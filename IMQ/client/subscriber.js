import * as net from "net";
import * as readline from "readline";
import SubscriberController from "./controller/subscriberController.js";
import { subscriberPORT, allTopics } from "./Constants.js";

const SubscriberHandler = new SubscriberController();
const subscriberConsoleInteractions = readline.createInterface({
  input: process.stdin,
});
const subscriberServerInteraction = new net.Socket();

subscriberServerInteraction.connect(subscriberPORT, process.argv[2], () => {
  console.log(`\nWelcome to IMQ Client Port\n`);
  console.log(`Please select the topic you want to pull message from\n`);
});

subscriberServerInteraction.on("data", (res) => {
  SubscriberHandler.responseFromServer(res, subscriberServerInteraction);
});

subscriberServerInteraction.on("error", (error) => {
  console.log("You are disconnected from the Server:" + error.message);
});

subscriberServerInteraction.write(JSON.stringify(allTopics));

subscriberConsoleInteractions.on("line", (message) => {
  SubscriberHandler.handleSubscriberEntries(subscriberServerInteraction, message, subscriberConsoleInteractions);
});

subscriberConsoleInteractions.on("close", () => {
  subscriberServerInteraction.end();
});
