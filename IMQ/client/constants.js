export const subscriberPORT = 59898;

export const publisherPORT = 59899;

export const allTopics = "getAllTopics";

export const publishMessage = "imq publish -m";

export const createTopic = "imq publish -c";

export const connectSubscriber = "imq connect to";

export const ternimate = "imq connect terminate";

export const invaliedMessage = "Invalied input";
