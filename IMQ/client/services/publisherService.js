import Shared from "../shared/shared.js";
import ServicesError from "../errorHandler/servicesErrorHandler.js";
import { allTopics } from "../constants.js";

const shared = new Shared();

let converter;
let numberOfTopics;
let data = {};

export default class PublisherService {
  processResponse(data, clientRequest) {
    converter = data;
    numberOfTopics = converter.length;
    try {
      converter[0].topic_id ? shared.printTopics(converter) : shared.printServerFullMessage(converter, clientRequest);
    } catch (error) {
      throw new ServicesError(`failed to process the response because ${error.message}`);
    }
  }

  publishMessage(client, message) {
    const topicId = shared.getTopicId(message);
    const messageToPublish = shared.getRequiredString(message);
    const messageSize = Buffer.from(messageToPublish).length;
    if (messageSize < 512) {
      if (topicId <= numberOfTopics && messageToPublish.length != 0) {
        try {
          data = {
            topicId: topicId,
            topicData: messageToPublish,
            addMessageToDb: true,
          };
          client.write(JSON.stringify(data));
        } catch (error) {
          throw new ServicesError(`failed to publishMessage because ${error.message}`);
        }
        console.log("Message sent to server\n");
      } else {
        console.log("Invalied entries");
      }
    } else {
      console.log("String size is exding the limit");
    }
  }

  createTopic(client, message) {
    const topicName = shared.getRequiredString(message).trim();
    if (topicName.length != 0) {
      numberOfTopics++;
      try {
        data = {
          topicId: numberOfTopics,
          topicName: topicName,
          newTopic: true,
        };
        client.write(JSON.stringify(data));
        client.write(JSON.stringify(allTopics));
        console.log(`\nNew Topic added with topicId "${data.topicId}" and topic name "${topicName}"\n`);
      } catch (error) {
        throw new ServicesError(`failed to createTopic because ${error.message}`);
      }
    } else {
      console.log("Topic name cannot be empty");
    }
  }
}
