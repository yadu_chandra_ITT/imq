import Shared from "../shared/shared.js";
import ServicesError from "../errorHandler/servicesErrorHandler.js";

const shared = new Shared();
let converter;
let topicLength;
let count;

export default class SubscriberService {
  processResponse(data, subscriberServerInteraction) {
    converter = JSON.parse(data.toString("utf-8"));
    if (converter.length > 0) {
      topicLength = converter.length;
      if (converter[0].topicName) {
        try {
          shared.printTopics(converter);
        } catch (error) {
          throw new ServicesError(`failed to printTopics because ${error.message}`);
        }
      }
    } else if (converter.data) {
      count = converter.data.length;
      try {
        const ack = {
          received: true,
        };
        shared.printMessages(converter);
        console.log(`There are ${count} messages.\n`);
        subscriberServerInteraction.write(JSON.stringify(ack));
      } catch (error) {
        throw new ServicesError(`failed to printMessages because ${error.message}`);
      }
    } else {
      console.log("No topics");
    }
  }

  subscriberMessage(client, message) {
    const serverMessage = shared.getTopicId(message);
    if (topicLength >= serverMessage) {
      const data = {
        topicId: serverMessage,
        getTopicMessages: true,
      };
      client.write(JSON.stringify(data));
    } else {
      console.log("Enter valied topic ID\n");
    }
  }
}
