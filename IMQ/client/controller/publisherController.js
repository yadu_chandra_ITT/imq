import PublisherService from "../services/publisherService.js";
import { publishMessage, createTopic, invaliedMessage, ternimate } from "../Constants.js";
import ControllerError from "../errorHandler/controllerErrorHandler.js";

const publisherservice = new PublisherService();

export default class PublisherController {
  handlePublisherEntries(publisherServerInteraction, publisherConsoleInteractions, message) {
    try {
      switch (true) {
        case message.includes(publishMessage):
          publisherservice.publishMessage(publisherServerInteraction, message);
          break;
        case message.includes(createTopic):
          publisherservice.createTopic(publisherServerInteraction, message);
          break;
        case message.includes(ternimate):
          publisherConsoleInteractions.close();
          break;
        default:
          console.log(invaliedMessage);
          break;
      }
    } catch (error) {
      throw new ControllerError(`failed to handle publisher request because ${error.message}`);
    }
  }

  responseFromServer(converter, publisherConsoleInteractions) {
    try {
      publisherservice.processResponse(converter, publisherConsoleInteractions);
    } catch (error) {
      throw new ControllerError(`failed to handle response from server because ${error.message}`);
    }
  }
}
