import SubscriberService from "../services/subscriberService.js";
import { connectSubscriber, ternimate, invaliedMessage } from "../Constants.js";
import ControllerError from "../errorHandler/controllerErrorHandler.js";

const subscriberService = new SubscriberService();

export default class SubscriberHandler {
  handleSubscriberEntries(subscriberServerInteraction, message, subscriberConsoleInteractions) {
    try {
      switch (true) {
        case message.includes(connectSubscriber):
          subscriberService.subscriberMessage(subscriberServerInteraction, message, subscriberConsoleInteractions);
          break;
        case message.includes(ternimate):
          subscriberConsoleInteractions.close();
          break;
        default:
          console.log(invaliedMessage);
          break;
      }
    } catch (error) {
      throw new ControllerError(`failed to handle subscriber request because ${error.message}`);
    }
  }

  responseFromServer(message, subscriberServerInteraction) {
    try {
      subscriberService.processResponse(message, subscriberServerInteraction);
    } catch (error) {
      throw new ControllerError(`failed to handle response from server because ${error.message}`);
    }
  }
}
