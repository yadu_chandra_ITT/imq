import gulp from "gulp";
import terser from "gulp-terser";
import rimraf from "rimraf";

gulp.task('clean', function (cb) {
    rimraf('dest\*', cb);
});

gulp.task('client', function(){
    return gulp.src(['client/**/*.js'])
        .pipe(terser())
        .pipe(gulp.dest("dist/client/"));
});

gulp.task('server', function(){
    return gulp.src(['server/**/*.js'])
        .pipe(terser())
        .pipe(gulp.dest("dist/server/"));
});

gulp.task('test', function(){
    return gulp.src(['test/**/*.js'])
        .pipe(terser())
        .pipe(gulp.dest("dist/test/"));
});

gulp.task('default', gulp.series('clean', 'server', 'client', 'test'));