import DataBaseService from "../services/databaseService.js";
import ServerService from "../services/serverService.js";
import { allTopics, deadLetterQueueTimeLimit } from "../Constants.js";
import Queue from "../queue/queue.js";
import ControllerError from "../errorHandlers/controllerErrorHandler.js";
import StringOperator from "../util/textOperator.js";

const dataBaseService = new DataBaseService();
const serverService = new ServerService();
const messageOperations = new StringOperator();
const queue = new Queue();

export default class serverController {
  handlePublisherRequests = async (message, connect, socket) => {
    const topicDetails = {
      topicId: message.topicId,
      topicMessage: message.topicData,
      timeStamp: new Date().toLocaleString(),
    };
    const createTopicDetails = {
      topic_id: message.topicId,
      topicName: message.topicName,
    };
    switch (true) {
      case message == allTopics:
        try {
          let topics = await dataBaseService.getAllTopics(connect);
          serverService.dataEcho(socket, JSON.stringify(topics));
        } catch (error) {
          throw new ControllerError(`failed to get all the topics for publisherbecause ${error.message}`);
        }
        break;
      case message.addMessageToDb:
        try {
          topicDetails.topicMessage = messageOperations.encrypt(topicDetails.topicMessage);
          await dataBaseService.addTopicDataToDb(topicDetails, connect);
        } catch (error) {
          throw new ControllerError(`failed to add message to db because ${error.message}`);
        }
        break;
      case message.newTopic:
        try {
          await dataBaseService.addNewTopicToDb(createTopicDetails, connect);
        } catch (error) {
          throw new ControllerError(`failed to create new topic because ${error.message}`);
        }
        break;
    }
  };

  handleSubscriberRequests = async (message, connect, socket) => {
    switch (true) {
      case message == allTopics:
        try {
          let topics = await dataBaseService.getAllTopics(connect);
          topics = JSON.stringify(topics);
          serverService.dataEcho(socket, topics);
        } catch (error) {
          throw new ControllerError(`failed to get topics for subscriber because ${error.message}`);
        }
        break;
      case message.getTopicMessages:
        try {
          let topicDetails = await dataBaseService.getTopicPublishedMessages(message.topicId, connect);
          topicDetails.map((item) => {
            item.message = messageOperations.decrypt(item.message);
            queue.enqueue(item);
          });
          topicDetails = JSON.stringify(queue);
          serverService.dataEcho(socket, topicDetails);
          setTimeout(async function () {
            if (queue.data.length > 0) {
              await dataBaseService.addItemsToDeadLetterQueue(queue.data, connect, socket);
            }
          }, deadLetterQueueTimeLimit);
        } catch (error) {
          throw new ControllerError(`failed to get messages of topic because ${error.message}`);
        }
        break;
      case message.received:
        console.log("All the messages received by client");
        queue.clear();
        break;
    }
  };

  handleClientDetails = async (socket, connect) => {
    var clientDetails = {
      clientName: `${socket.remoteAddress.slice(-9)}_${socket.remotePort}`,
    };
    try {
      const clientCount = await dataBaseService.checkForClient(clientDetails, connect);
      if (clientCount.length === 0) {
        await dataBaseService.addClientToReference(clientDetails, connect);
      }
    } catch (error) {
      throw new ControllerError(`failed to handle client request because ${error.message}`);
    }
  };

  getDisconnectMessage = (socket) => {
    serverService.diconnectionMessage(socket);
  };
}
