import * as net from "net";
import MongoDbConnection from "./Database/MongoDbConnection.js";
import { subscriberPORT, publisherPORT, subscriberLimit, publisherLimit, limitExedMessage, databaseName } from "./Constants.js";
import serverController from "./Controller/serverControllers.js";

const connection = new MongoDbConnection();
const serverHandler = new serverController();
let connect;
let numberOfSubscriber = 0;
let numberOfPublisher = 0;

(async function () {
  connect = await connection.connectDb(databaseName);
})();

const imqSubscriberServer = net.createServer(async (socket) => {
  numberOfSubscriber++;
  console.log(`\nA new client with ${socket.remoteAddress.slice(-9)} and port ${socket.remotePort} connected to the server`);

  if (numberOfSubscriber <= subscriberLimit) {
    socket.on("data", async (buffer) => {
      let message = JSON.parse(buffer.toString("utf-8"));
      //   serverHandler.handleClientDetails(socket, connect);
      serverHandler.handleSubscriberRequests(message, connect, socket);
    });
  }

  socket.on("end", () => {
    numberOfSubscriber--;
    serverHandler.getDisconnectMessage(socket);
  });

  socket.on("error", () => {
    numberOfSubscriber--;
    serverHandler.getDisconnectMessage(socket);
  });
});

const imqPublisherServer = net.createServer((socket) => {
  numberOfPublisher++;
  console.log(`\nA new publisher with ${socket.remoteAddress.slice(-9)} and port ${socket.remotePort} connected to the server`);
  if (numberOfPublisher <= publisherLimit) {
    socket.on("data", async (buffer) => {
      let message = JSON.parse(buffer.toString("utf-8"));
      //   serverHandler.handleClientDetails(socket, connect);
      serverHandler.handlePublisherRequests(message, connect, socket);
    });
  } else {
    numberOfPublisher--;
    socket.write(JSON.stringify(limitExedMessage));
  }

  socket.on("end", () => {
    numberOfPublisher--;
    serverHandler.getDisconnectMessage(socket);
  });

  socket.on("error", () => {
    numberOfPublisher--;
    serverHandler.getDisconnectMessage(socket);
  });
});

imqSubscriberServer.listen(subscriberPORT, async () => {
  console.log(`server is live on port ${subscriberPORT}`);
});

imqPublisherServer.listen(publisherPORT, () => {
  console.log(`server is live on port ${publisherPORT}`);
});
