import fileOperation from "../fileCreation/index.js";

export default class ServerService {
  addMessageToClientFile(socket, message) {
    fileOperation(socket, message);
  }

  dataEcho(socket, message) {
    socket.write(`${message}`);
  }

  diconnectionMessage(socket) {
    console.log(`A Client with IP ${socket.remoteAddress.slice(-9)} and port ${socket.remotePort} disconnected from the server`);
  }
}
