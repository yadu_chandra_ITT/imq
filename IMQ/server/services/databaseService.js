import MessageManager from "../Database/databaseLayers/messageManager.js";
import ClientListManager from "../Database/databaseLayers/clientReferenceManager.js";
import ServicesError from "../errorHandlers/servicesErrorHandler.js";

const messageManager = new MessageManager();
const clientListManager = new ClientListManager();

export default class DataBaseService {
  checkForClient = async (clientDetails, dbo) => {
    try {
      const clientCoun = await clientListManager.checkInReferenceInTable(clientDetails, dbo);
      return clientCoun;
    } catch (error) {
      throw new ServicesError(`failed to check client reference because ${error.message}`);
    }
  };

  addClientToReference = async (clientDetails, dbo) => {
    try {
      await clientListManager.addClientInReferenceTable(clientDetails, dbo).then((message) => {
        console.log(message);
      });
    } catch (error) {
      throw new ServicesError(`failed to add to client reference because ${error.message}`);
    }
  };

  addTopicDataToDb = async (topicDetails, dbo) => {
    try {
      await messageManager.addNewMessageToDb(topicDetails, dbo).then((res) => {
        console.log(res);
      });
    } catch (error) {
      throw new ServicesError(`failed to add topic data because ${error.message}`);
    }
  };

  addNewTopicToDb = async (newTopicDetails, dbo) => {
    try {
      await messageManager.addDataToTopicCollection(newTopicDetails, dbo).then((res) => {
        console.log(res);
      });
    } catch (error) {
      throw new ServicesError(`failed to add new topic because ${error.message}`);
    }
  };

  getTopicPublishedMessages = async (message, dbo) => {
    try {
      const data = await messageManager.getTopicdetails(message, dbo);
      return data;
    } catch (error) {
      throw new ServicesError(`failed to get messages of topic because ${error.message}`);
    }
  };

  getAllTopics = async (dbo) => {
    try {
      const data = await messageManager.getTopics(dbo);
      return data;
    } catch (error) {
      throw new ServicesError(`failed to get topics because ${error.message}`);
    }
  };

  addItemsToDeadLetterQueue = async (items, dbo, socket) => {
    try {
      await messageManager.addDeadLetterItemsDb(items, dbo, socket).then((res) => {
        console.log(res);
      });
    } catch (error) {
      throw new ServicesError(`failed to add topic data because ${error.message}`);
    }
  };
}
