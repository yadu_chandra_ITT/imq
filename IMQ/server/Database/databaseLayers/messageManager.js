import DataBaseoperations from "../databaseOperations/operations.js";
import { messageCollection, referenceTopicName, deadLetterQueueCollection } from "../../Constants.js";
import DatabaseError from "../../errorHandlers/dbErrorHandler.js";

const databaseOperations = new DataBaseoperations();

export default class MessageManager {
  addDataToTopicCollection = async (newTopicDetails, dbo) => {
    const collectionName = referenceTopicName;
    try {
      await databaseOperations.addToDb(newTopicDetails, collectionName, dbo);
      var ack = `Topic added`;
    } catch (error) {
      throw new DatabaseError(`failed to add data to topics collection because ${error.message}`);
    }
    return ack;
  };

  addNewMessageToDb = async (topicDetails, dbo) => {
    const data = { topicId: topicDetails.topicId, message: topicDetails.topicMessage, timeStamp: topicDetails.timeStamp };
    const collectionName = messageCollection;
    try {
      await databaseOperations.addToDb(data, collectionName, dbo);
      var ack = `Data added to topic with topicID ${topicDetails.topicId}`;
    } catch (error) {
      throw new DatabaseError(`failed to add message because ${error.message}`);
    }
    return ack;
  };

  addDeadLetterItemsDb = async (items, dbo, socket) => {
    const data = { clientId: `${socket.remoteAddress.slice(-9)}_${socket.remotePort}`, messageCollection: items };
    const collectionName = deadLetterQueueCollection;
    try {
      await databaseOperations.addToDb(data, collectionName, dbo);
      var ack = `items added to dead letter queue`;
    } catch (error) {
      throw new DatabaseError(`failed to add message because ${error.message}`);
    }
    return ack;
  };

  getTopicdetails = async (message, dbo) => {
    const collectionName = messageCollection;
    try {
      var data = await databaseOperations.findTopicDetailsInDb(message, dbo, collectionName);
    } catch {
      throw new DatabaseError(`failed to get topics details because ${error.message}`);
    }
    return data;
  };

  getTopics = async (dbo) => {
    try {
      var allTopics = await databaseOperations.getTopicsFromDb(dbo, referenceTopicName);
    } catch {
      throw new DatabaseError(`failed to get topics because ${error.message}`);
    }
    return allTopics;
  };
}
