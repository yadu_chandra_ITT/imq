import { referenceCollectionName } from "../../Constants.js";
import DataBaseoperations from "../databaseOperations/operations.js";
import DatabaseError from "../../errorHandlers/dbErrorHandler.js";

const databaseOperations = new DataBaseoperations();

export default class ClientListManager {
  checkInReferenceInTable = async (clientDetails, dbo) => {
    try {
      const existingClient = await databaseOperations.checkForClientDetails(clientDetails, dbo, referenceCollectionName);
      return existingClient;
    } catch {
      throw new DatabaseError(`failed to get client refrence because ${error.message}`);
    }
  };

  addClientInReferenceTable = async (clientDetails, dbo) => {
    const data = { clientName: clientDetails.clientName };
    try {
      await databaseOperations.addToDb(data, referenceCollectionName, dbo);
      var ack = `${clientDetails.clientName} added to referenceTable`;
      return ack;
    } catch (error) {
      throw new DatabaseError(`failed to add client to because ${error.message}`);
    }
  };
}
