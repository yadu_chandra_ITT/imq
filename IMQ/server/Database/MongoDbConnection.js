import mongodb from "mongodb";
import { url } from "../Constants.js";

export default class MongoDbConnection {
  MongoClient = mongodb.MongoClient;

  async connectDb(databaseName) {
    return new Promise((resolve, reject) => {
      try {
        this.MongoClient.connect(url, { useUnifiedTopology: true }, async (err, db) => {
          var dbo = db.db(databaseName);
          resolve(dbo);
        });
      } catch (e) {
        reject(e);
      }
    });
  }
}
