import DatabaseError from "../../errorHandlers/dbErrorHandler.js";

export default class DataBaseoperations {
  addToDb = async (data, collectionName, dbo) => {
    try {
      const ack = await dbo.collection(collectionName).insertOne(data);
      return ack;
    } catch (error) {
      throw new DatabaseError(`failed to add data to db because ${error.message}`);
    }
  };

  checkForClientDetails = async (clientDetails, dbo, collectionName) => {
    try {
      const data = await dbo
        .collection(collectionName)
        .find({ clientName: `${clientDetails.clientName}` })
        .toArray();
      return data;
    } catch (error) {
      throw new DatabaseError(`failed to check client details because ${error.message}`);
    }
  };

  getTopicsFromDb = async (dbo, collectionName) => {
    try {
      const data = await dbo.collection(collectionName).find({}).toArray();
      return data;
    } catch (error) {
      throw new DatabaseError(`failed to get topics because ${error.message}`);
    }
  };

  findTopicDetailsInDb = async (message, dbo, collectionName) => {
    const temp = {
      topicId: `${message.replace(/\n/g, "")}`,
    };
    try {
      const data = await dbo.collection(collectionName).find(temp).toArray();
      return data;
    } catch (error) {
      throw new DatabaseError(`failed to get topics details because ${error.message}`);
    }
  };
}
