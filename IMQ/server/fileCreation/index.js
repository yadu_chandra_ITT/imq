import { appendFileSync } from "fs";

function saveDataToFile(socket, message) {
  try {
    appendFileSync(
      `../server/files/${socket.remoteAddress.slice(-9)}_${socket.remotePort}`,
      `${message}  ${new Date().toLocaleString()} \n`
    );
  } catch (error) {
    console.error(error);
  }
}

export default saveDataToFile;
