export const subscriberPORT = 59898;
export const publisherPORT = 59899;

export const allTopics = "getAllTopics";

export const url = "mongodb://localhost:27017";

export const referenceCollectionName = "IMQclientsReference";

export const messageCollection = "IMQmessageCollection";

export const deadLetterQueueCollection = "IMQdeadLetterQueueCollection";

export const referenceTopicName = "IMQ_Topics";

export const databaseName = "IMQ-database";

export const subscriberLimit = 2;

export const publisherLimit = 2;

export const queueSizeLimit = 10;

export const deadLetterQueueTimeLimit = 5000;

export const limitExedMessage = "No slot in the server please try after some time";
